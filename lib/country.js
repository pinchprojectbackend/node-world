function Country(identifiers, data) {
    this.alpha2Code = identifiers.alpha_2_code;
    this.alpha3code = identifiers.alpha_3_code;
    this.name = identifiers.name;

    this.localizedNames = data.localized_names;
}

Country.prototype.getName = function () {
    return this.name;
};

Country.prototype.getAlpha2Code = function () {
    return this.alpha2Code;
};

Country.prototype.getAlpha3Code = function () {
    return this.alpha3code;
};

Country.prototype.getCodes = function () {
    return {
        alpha_2_code: this.alpha2Code,
        alpha_3_code: this.alpha3code
    };
};

Country.prototype.getLocalizedNames = function () {
    return this.localizedNames;
};

Country.prototype.getLocalizedNameIn = function (language) {
    var self = this,
        name = self.localizedNames[language.toLowerCase()];

    if (!name)
        name = self.name;

    return name;
};

module.exports = Country;