var FileLoader = require('./file_loader'),
    Country = require('./country');

var countries = FileLoader.load('countries');

exports.findCountryByCode = function (code) {
    var found = false,
        item;

    for (var i = 0; i < countries.length; i++) {
        item = countries[i];

        if (item.alpha_2_code === code.toUpperCase() || item.alpha_3_code === code.toUpperCase()) {
            found = true;
            break;
        }
    }

    if (!found) return;

    var countryData = FileLoader.load('countries/' + item.alpha_2_code.toLowerCase() + '_' + item.alpha_3_code.toLowerCase());
    var country = new Country(item, countryData);
    return country;
};

exports.findCountryByName = function (name) {
    var found = false,
        item;

    for (var i = 0; i < countries.length; i++) {
        item = countries[i];

        if (item.name.toLowerCase() === name.toLowerCase()) {
            found = true;
            break;
        }
    }

    if (!found) return;

    var countryData = FileLoader.load('countries/' + item.alpha_2_code.toLowerCase() + '_' + item.alpha_3_code.toLowerCase());
    var country = new Country(item, countryData);
    return country;
};